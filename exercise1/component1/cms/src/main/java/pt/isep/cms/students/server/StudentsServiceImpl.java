package pt.isep.cms.students.server;

import com.google.gwt.user.client.Window;
import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import pt.isep.cms.students.client.StudentsService;
import pt.isep.cms.students.shared.Student;
import pt.isep.cms.students.shared.StudentDetails;

@SuppressWarnings("serial")
public class StudentsServiceImpl extends RemoteServiceServlet implements
    StudentsService {

  private static final String[] studentsNameData = new String[] {
      "Hollie", "Emerson", "Healy", "Brigitte", "Elba", "Claudio",
      "Dena", "Christina", "Gail", "Orville", "Rae", "Mildred",
      "Candice", "Louise", "Emilio", "Geneva", "Heriberto", "Bulrush", 
      "Abigail", "Chad", "Terry", "Bell"};
  
  private final String[] studentsGenderData = new String[] {
      "female", "male", "female", "female", "female", "male",
      "female", "female", "male", "male", "male", "male",
      "female", "female", "male", "female", "male", "male", 
      "female", "male", "male", "female"};
  
  private final String[] studentsBirthdayData = new String[] {
      "11/11/1996", "11/11/1996", "11/11/1996",
      "12/11/1996", "12/11/1996", "12/11/1996",
      "13/11/1996", "13/11/1996", "13/11/1996",
      "14/11/1996", "14/11/1996", "14/11/1996",
      "15/11/1996", "15/11/1996",
      "15/11/1996", "16/11/1996", "16/11/1996",
      "16/11/1996", "17/11/1996", "17/11/1996",
      "17/11/1996", "18/11/1996"};
      
  private final HashMap<String, Student> students = new HashMap<String, Student>();

  public StudentsServiceImpl() {
    initStudents();
  }
  
  private void initStudents() {
    // TODO: Create a real UID for each student
    //
    for (int i = 0; i < studentsNameData.length && i < studentsGenderData.length && i < studentsBirthdayData.length; ++i) {
      Student student = new Student(String.valueOf(i), studentsNameData[i], studentsGenderData[i], studentsBirthdayData[i]);
      students.put(student.getId(), student); 
    }
  }
  
  public Student addStudent(Student student) {
    student.setId(String.valueOf(students.size()));
    students.put(student.getId(), student); 
    return student;
  }

  public Student updateStudent(Student student) {
	  String lid=student.getId();
	  students.remove(student.getId());
    students.put(student.getId(), student); 
    return student;
  }

  public Boolean deleteStudent(String id) {
	  students.remove(id);
    return true;
  }
  
  public ArrayList<StudentDetails> deleteStudents(ArrayList<String> ids) {

    for (int i = 0; i < ids.size(); ++i) {
      deleteStudent(ids.get(i));
    }
    
    return getStudentDetails();
  }
  
  public ArrayList<StudentDetails> getStudentDetails() {
    ArrayList<StudentDetails> studentDetails = new ArrayList<StudentDetails>();
    
    Iterator<String> it = students.keySet().iterator();
    while(it.hasNext()) { 
    	Student student = students.get(it.next());          
      studentDetails.add(student.getLightWeightStudent());
    }

    return studentDetails;
  }

  public Student getStudent(String id) {
    return students.get(id);
  }
}
