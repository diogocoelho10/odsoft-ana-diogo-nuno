package pt.isep.cms.teachers.shared;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Teacher implements Serializable {
	public String idNumber;
  public String firstName;
  public String lastName;
  public String gender;
  public String birthDate;
	
	public Teacher() {}
	
	public Teacher(String idNumber, String firstName, String lastName, String gender, String birthDate) {
		this.idNumber = idNumber;
    this.firstName = firstName;
    this.lastName = lastName;
    this.gender = gender;
		this.birthDate = birthDate;
	}
	
	public TeacherDetails getLightWeightTeacher() {
	  return new TeacherDetails(idNumber, getFullName());
	}
	
  public String getId() { return idNumber; }
  public void setId(String idNumber) { this.idNumber = idNumber; }
  public String getFirstName() { return firstName; }
  public void setFirstName(String firstName) { this.firstName = firstName; }
  public String getLastName() { return lastName; }
  public void setLastName(String lastName) { this.lastName = lastName; }
  public String getGender() { return gender; }
  public void setGender(String gender) { this.gender = gender; }
  public String getBirthDate() { return birthDate; }
  public void setBirthDate(String birthDate) { this.birthDate = birthDate; }
  public String getFullName() { return firstName + " " + lastName; }

}
