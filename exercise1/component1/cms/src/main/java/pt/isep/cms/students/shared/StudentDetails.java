package pt.isep.cms.students.shared;

import java.io.Serializable;

@SuppressWarnings("serial")
public class StudentDetails implements Serializable {
  private String id;
  private String name;
  
  public StudentDetails() {
    new StudentDetails("0", "");
  }

  public StudentDetails(String id, String name) {
    this.id = id;
    this.name = name;
  }
  
  public String getId() { return id; }
  public void setId(String id) { this.id = id; }
  public String getName() { return name; }
  public void setName(String name) { this.name = name; } 
}