package pt.isep.cms.teachers.client.presenter;

import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.HasValue;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.user.client.Window;
import pt.isep.cms.teachers.client.TeachersServiceAsync;
import pt.isep.cms.teachers.client.event.TeacherUpdatedEvent;
import pt.isep.cms.teachers.client.event.EditTeacherCancelledEvent;
import pt.isep.cms.teachers.shared.Teacher;

public class EditTeachersPresenter implements Presenter {
	public interface Display {
		HasClickHandlers getSaveButton();

		HasClickHandlers getCancelButton();

		HasValue<String> getFirstName();

		HasValue<String> getLastName();

		HasValue<String> getGender();
		
		HasValue<String> getBirthDate();

		void show();

		void hide();
	}

	private Teacher teacher;
	private final TeachersServiceAsync rpcService;
	private final HandlerManager eventBus;
	private final Display display;

	public EditTeachersPresenter(TeachersServiceAsync rpcService, HandlerManager eventBus, Display display) {
		this.rpcService = rpcService;
		this.eventBus = eventBus;
		this.teacher = new Teacher();
		this.display = display;
		bind();
	}

	public EditTeachersPresenter(TeachersServiceAsync rpcService, HandlerManager eventBus, Display display, String id) {
		this.rpcService = rpcService;
		this.eventBus = eventBus;
		this.display = display;
		bind();

		rpcService.getTeacher(id, new AsyncCallback<Teacher>() {
			public void onSuccess(Teacher result) {
				teacher = result;
				EditTeachersPresenter.this.display.getFirstName().setValue(teacher.getFirstName());
				EditTeachersPresenter.this.display.getLastName().setValue(teacher.getLastName());
				EditTeachersPresenter.this.display.getGender().setValue(teacher.getGender());
				EditTeachersPresenter.this.display.getBirthDate().setValue(teacher.getBirthDate());
			}

			public void onFailure(Throwable caught) {
				Window.alert("Error retrieving teacher");
			}
		});

	}

	public void bind() {
		this.display.getSaveButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				doSave();
				display.hide();
			}
		});

		this.display.getCancelButton().addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				display.hide();
				eventBus.fireEvent(new EditTeacherCancelledEvent());
			}
		});
	}

	public void go(final HasWidgets container) {
		display.show();
	}

	private void doSave() {
		teacher.setFirstName(display.getFirstName().getValue());
		teacher.setLastName(display.getLastName().getValue());
		teacher.setGender(display.getGender().getValue());
		teacher.setBirthDate(display.getBirthDate().getValue());
		
		if (teacher.getId() == null) {
			// Adding new contact
			rpcService.addTeacher(teacher, new AsyncCallback<Teacher>() {
				public void onSuccess(Teacher result) {
					eventBus.fireEvent(new TeacherUpdatedEvent(result));
				}

				public void onFailure(Throwable caught) {
					Window.alert("Error adding teacher");
				}
			});
		} else {
			// updating existing teacher
			rpcService.updateTeacher(teacher, new AsyncCallback<Teacher>() {
				public void onSuccess(Teacher result) {
					eventBus.fireEvent(new TeacherUpdatedEvent(result));
				}

				public void onFailure(Throwable caught) {
					Window.alert("Error updating teacher");
				}
			});
		}
	}

}
