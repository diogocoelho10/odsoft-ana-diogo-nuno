package pt.isep.cms.teachers.server;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import pt.isep.cms.teachers.client.TeachersService;
import pt.isep.cms.teachers.shared.Teacher;
import pt.isep.cms.teachers.shared.TeacherDetails;

@SuppressWarnings("serial")
public class TeachersServiceImpl extends RemoteServiceServlet implements
	TeachersService {

  private static final String[] teachersFirstNameData = new String[] {
      "Hollie", "Emerson", "Healy", "Brigitte", "Elba", "Claudio",
      "Dena", "Christina", "Gail", "Orville", "Rae", "Mildred",
      "Candice", "Louise", "Emilio", "Geneva", "Heriberto", "Bulrush", 
      "Abigail", "Chad", "Terry", "Bell"};
  
  private final String[] teachersLastNameData = new String[] {
      "Voss", "Milton", "Colette", "Cobb", "Lockhart", "Engle",
      "Pacheco", "Blake", "Horton", "Daniel", "Childers", "Starnes",
      "Carson", "Kelchner", "Hutchinson", "Underwood", "Rush", "Bouchard", 
      "Louis", "Andrews", "English", "Snedden"};
  
  private final String[] teachersBirthDateData = new String[] {
	      "12/02/1997", "13/01/1990", "24/10/1987", "24/10/1987", "24/10/1987", "24/10/1987",
	      "24/10/1987", "24/10/1987", "24/10/1987", "24/10/1987", "24/10/1987", "24/10/1987",
	      "24/10/1987", "24/10/1987", "24/10/1987", "24/10/1987", "24/10/1987", "24/10/1987", 
	      "24/10/1987", "24/10/1987", "24/10/1987", "24/10/1987"};
	  
  private final String[] teachersGenderData = new String[] {
      "Masculino", "Feminino"};
      
  private final HashMap<String, Teacher> teachers = new HashMap<String, Teacher>();

  public TeachersServiceImpl() {
    initTeachers();
  }
  
  private void initTeachers() {
    // TODO: Create a real UID for each teacher
    //
    for (int i = 0; i < teachersFirstNameData.length && i < teachersLastNameData.length && i < teachersGenderData.length && i < teachersBirthDateData.length; ++i) {
    	Teacher teacher = new Teacher(String.valueOf(i), teachersFirstNameData[i], teachersLastNameData[i], teachersGenderData[i], teachersBirthDateData[i]);
      teachers.put(teacher.getId(), teacher); 
    }
  }
  
  public Teacher addTeacher(Teacher teacher) {
    teacher.setId(String.valueOf(teachers.size()));
    teachers.put(teacher.getId(), teacher); 
    return teacher;
  }

  public Teacher updateTeacher(Teacher teacher) {
	  String lid=teacher.getId();
    teachers.remove(teacher.getId());
    teachers.put(teacher.getId(), teacher); 
    return teacher;
  }

  public Boolean deleteTeacher(String id) {
    teachers.remove(id);
    return true;
  }
  
  public ArrayList<TeacherDetails> deleteTeachers(ArrayList<String> ids) {

    for (int i = 0; i < ids.size(); ++i) {
      deleteTeacher(ids.get(i));
    }
    
    return getTeacherDetails();
  }
  
  public ArrayList<TeacherDetails> getTeacherDetails() {
    ArrayList<TeacherDetails> teacherDetails = new ArrayList<TeacherDetails>();
    
    Iterator<String> it = teachers.keySet().iterator();
    while(it.hasNext()) { 
    	Teacher teacher = teachers.get(it.next());          
      teacherDetails.add(teacher.getLightWeightTeacher());
    }
    System.out.println("ENTRAAaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaAAAAAAAAAAAAAAAAAAA222222222");
    return teacherDetails;
  }

  public Teacher getTeacher(String id) {
    return teachers.get(id);
  }
  
  
}
