package pt.isep.cms.teachers.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.junit.client.GWTTestCase;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.rpc.ServiceDefTarget;
import pt.isep.cms.teachers.client.TeachersService;
import pt.isep.cms.teachers.client.TeachersServiceAsync;
import pt.isep.cms.teachers.client.presenter.TeachersPresenter;
import pt.isep.cms.teachers.client.view.TeachersView;
import pt.isep.cms.teachers.shared.Teacher;
import pt.isep.cms.teachers.shared.TeacherDetails;

// Nao se pode usar o easymock com testes GWT pois este usar reflection e o GWT não consegue "transpile"....
//import static org.easymock.EasyMock.createStrictMock;

import java.util.ArrayList;

public class ExampleGWTTest extends GWTTestCase {
	private TeachersPresenter teachersPresenter;
	private TeachersServiceAsync rpcService;
	private HandlerManager eventBus;
	private TeachersPresenter.Display mockDisplay;

	public String getModuleName() {
		return "pt.isep.cms.teachers.TestCMSJUnit";
	}

	public void gwtSetUp() {
		rpcService = GWT.create(TeachersService.class);
		mockDisplay = new TeachersView();
		teachersPresenter = new TeachersPresenter(rpcService, eventBus, mockDisplay);
	}

	public void testTeacherSort() {
		ArrayList<TeacherDetails> teacherDetails = new ArrayList<TeacherDetails>();
		teacherDetails.add(new TeacherDetails("0", "c_teacher"));
		teacherDetails.add(new TeacherDetails("1", "b_teacher"));
		teacherDetails.add(new TeacherDetails("2", "a_teacher"));
		teachersPresenter.setTeacherDetails(teacherDetails);
		teachersPresenter.sortTeacherDetails();
		assertTrue(teachersPresenter.getTeacherDetail(0).getDisplayName().equals("a_teacher"));
		assertTrue(teachersPresenter.getTeacherDetail(1).getDisplayName().equals("b_teacher"));
		assertTrue(teachersPresenter.getTeacherDetail(2).getDisplayName().equals("c_teacher"));
	}

	public void testTeachersService() {
		// Create the service that we will test.
		TeachersServiceAsync teachersService = GWT.create(TeachersService.class);
		ServiceDefTarget target = (ServiceDefTarget) teachersService;
		target.setServiceEntryPoint(GWT.getModuleBaseURL() + "teachers/teachersService");

		// Since RPC calls are asynchronous, we will need to wait for a response
		// after this test method returns. This line tells the test runner to wait
		// up to 10 seconds before timing out.
		delayTestFinish(10000);

		// fail("Ainda nao implementado");

		// Send a request to the server.
		teachersService.getTeacher("2", new AsyncCallback<Teacher>() {
			public void onFailure(Throwable caught) {
				// The request resulted in an unexpected error.
				fail("Request failure: " + caught.getMessage());
			}

			public void onSuccess(Teacher result) {
				// Verify that the response is correct.
				assertTrue(result != null);

				// Now that we have received a response, we need to tell the test runner
				// that the test is complete. You must call finishTest() after an
				// asynchronous test finishes successfully, or the test will time out.
				finishTest();
			}
		});
	}
}
