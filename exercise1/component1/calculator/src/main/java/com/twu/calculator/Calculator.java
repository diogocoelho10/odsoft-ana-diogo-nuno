package com.twu.calculator;

public class Calculator {

    private double accumulator;
    
    public static double factorial(int n) {
		if(n == 1) {
			return 1;
		}               
		return n * factorial(n-1);
	}

    public double doOperation(String operation, double operand) {
        switch (operation) {
            case "add":
                accumulator += operand;
                break;
            case "subtract":
                accumulator -= operand;
                break;
            case "multiply":
                accumulator *= operand;
                break;
            case "divide":
                accumulator /= operand;
                break;
            case "abs":
                accumulator = Math.abs(accumulator);
                break;
            case "neg":
                accumulator = -accumulator;
                break;
            case "sqrt":
                accumulator = Math.sqrt(accumulator);
                break;
            case "sqr":
                accumulator = Math.pow(accumulator, 2);
                break;
            case "cube":
                accumulator = Math.pow(accumulator, 3);
                break;
            case "cubert":
                accumulator = Math.cbrt(accumulator);
                break;
            case "factorial":
                accumulator = factorial(accumulator);
                break;
            case "third":
                accumulator = accumulator / 3.0;
                break;
            case "double":
                accumulator = accumulator * 2.0;
                break;
            case "exponential":
                accumulator = Math.pow(accumulator, operand);
                break;
            case "cancel":
                accumulator = 0;
                break;
            case "exit":
                System.exit(0);
        }
        return accumulator;
    }
    public static double factorial(double n) {
		if(n == 1) {
			return 1;
		}               
		return n * factorial(n-1);
	}
   
}
