## ODSOFT - Exercise 1 ##

### Calculator ###

#### Changes in Project Code 

- Added new options to switch inside the Calculator file.

#### PlantUML Diagrams

- A new renderPlantUml task was added to gradle.build;
- It generates .png files based on the diagrams that exist on the puml folder;
- Command to run renderPlantUml: gradlew renderPlantUml.

#### Javadoc 

- A new javadoc task was added to gradle.build;
- Command to run javadoc: gradlew javadoc.

#### Unit Tests 

- Command to run unit tests: gradlew test.

### CMS ###

#### Changes in Project Code 

- Specified the paths for translatable code source in Showcase.gwt.xml:	
	<source path="teachers/client"/>
	<source path="teachers/shared"/>
- Added to the MainMenuTreeViewModel a new CW Panel for Teachers;
- Created base teachers packages;
- Added a new servlet for the students in file web.xml.

#### Jenkins  

- Plugins reccomended installed
- A job was created as a freestyle project and connected to the repository.

#### War File 

- We had several problems;

#### PlantUML Diagrams

- A new renderPlantUml task was added to gradle.build;
- It generates .png files based on the diagrams that exist on the puml folder;
- Command to run renderPlantUml: gradlew renderPlantUml

#### Javadoc 

- A new javadoc task was added to gradle.build;
- Command to run javadoc: gradlew javadoc.

#### Unit Tests 

- Command to run test and coverage: gradlew test; gradlew jacocoTestReport.

#### Integration Tests 

- Command to run test and coverage: gradlew integrationTest ; gradlew jacocoIntegrationReport